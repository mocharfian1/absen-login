'use strict';

var response = require('../res');
var connection = require('../conn');

const request = require('request');

const TokenGenerator = require('uuid-token-generator');
const genToken = new TokenGenerator();

var infoPegawai = null;
var infoSession = null;
var global_username;

exports.setUsername = function (v_username){
    global_username = v_username;
}

exports.getUserInfo = function (token=null){
    return new Promise((resolve,reject)=>{
        var query = "select * from maps_user_session where token = '"+token+"'";

        connection.query(query, (err,result)=>{
            if(err){
                return reject("Bad Credentials");
            }else{
                if(result.length>0){
                    return resolve(result[0]);
                }else{
                    return reject("Not Found");
                }
            }
        });
    });

}

exports.getSessionInfo = function (username=null,password=null,callback=null,responseBack=null){
    response.logging(global_username,new Date(),' --> getSessionInfo');
    return new Promise((resolve,reject) =>{
        var query = `   SELECT 
                        mp.nip,mp.password,mp.satuanorganisasiid, mu.expire, mu.token
                    FROM 
                        maps_pegawai mp
                        LEFT JOIN maps_user_session mu ON (mp.nip=mu.nip OR mp.niplama=mu.nip) AND DATE(mu.expire)> DATE(NOW())
                    WHERE (mp.nip='`+ username + `' OR mp.niplama='` + username + `')`;


        connection.query(query, (err,result)=>{
            response.logging(global_username,new Date(),' --> getSessionInfo --> conn.query');
            if(result.length > 0){
                if(result[0].password == password){
                    if(result[0].nip === username && result[0].token === null){
                        response.logging(global_username,new Date(),' --> getSessionInfo --> conn.query --> ( Inserting Session User )');

                        this.getFromBKD_only_data(username,responseBack).then((res)=>{
                            // var jsonRes = JSON.parse(res);
                            var resp = {
                                success:true,
                                message:'Berhasil login.',
                                result:res
                            }

                            return resolve(resp);
                        });
                    }else{
                        return resolve({
                            success:false,
                            message:'Anda telah login di perangkat lain. Silakan hubungi operator.',
                            result:{}
                        });
                    }
                }else{
                    return resolve({
                        success:false,
                        message:'Username / Password salah.',
                        result:{}
                    });
                }
            }else{
                response.logging(global_username,new Date()," --> getSessionInfo --> conn.query --> result < 0");
                this.getFromBKD(username,password,null,responseBack).then((res)=>{
                    return resolve(res);
                });
            }
        });
    });
}

function getPola(kode=null,callback=null){
    var query = "select * from maps_pola_satuan_organisasi where kode = '" + kode + "'";

    connection.query(query,(err,result)=>{
        callback(err,result);
    });
}

exports.getFromBKD = function (username=null,password=null, callback=null,responseBack=null){
    response.logging(global_username,new Date(),' --> getFromBKD');

    return new Promise((resolve,reject) => {

        var query = "select * from v_data where NIP='" + username + "' or NIPbaru='" + username + "'";
        request.post('http://simpegdashboard.sragenkab.go.id/api/run_all',
            {form: {query: query}}, function (error, responseAll, body) {
                try {
                    if (body.length > 0) {
                        var res = JSON.parse(body)[0];
                        var insertQuery = null;
                        var nipActive = null;
                        infoPegawai = res;

                        getPola(res.SatuanOrganisasiID,(err,data)=>{

                            if(err){

                                response.logging(global_username,new Date(),' --> [ERROR] inserting to DB Pegawai');
                                response.submitLogin(global_username,false,"Gagal Login. ErrCode NULL [POLA_SATUAN_ORGANISASI]",responseBack);

                            }else{
                                if(data.length>0){

                                    if (res.NIPBaru === null) {
                                        insertQuery = `INSERT INTO maps_pegawai (nip, password, satuanorganisasiid,pola) VALUES 
                                                    ('` + res.NIP + `','` + password + `', '` + res.SatuanOrganisasiID + `','`+data[0].id_pola+`')`;
                                        nipActive = res.NIP;
                                    } else {
                                        insertQuery = ` INSERT INTO maps_pegawai (nip,niplama, password, satuanorganisasiid,pola) VALUES
                                                ('` + res.NIPBaru + `','` + res.NIP + `', '` + password + `', '` + res.SatuanOrganisasiID + `','`+data[0].id_pola+`)`;
                                        nipActive = res.NIPBaru;
                                    }

                                    response.logging(global_username,new Date(),' --> inserting to DB Pegawai');
                                    createSession(nipActive);
                                    insertToDBPegawai(insertQuery,responseBack);

                                }else{
                                    var responseDB = {
                                        success: false,
                                        message: "Gagal Login. ErrCode NULL [POLA_SATUAN_ORGANISASI].",
                                        result: null
                                    }

                                    response.logging(global_username,new Date(),"Gagal Login. ErrCode NULL [POLA_SATUAN_ORGANISASI]");
                                    return resolve(responseDB);
                                }

                            }
                        });


                    } else {
                        response.logging(global_username,new Date(),' --> errorParsing');
                        var responseDB = {
                            success: false,
                            message: "Data tidak ditemukan.",
                            result: null
                        }

                        return resolve(responseDB);
                    }
                } catch (e) {
                    console.log(e);
                }

            });
    });
}

exports.getFromBKD_only_data = function (username=null,responseBack=null){
    createSession(username);
    return new Promise((resolve,reject) => {

        var query = "select * from v_data where NIP='" + username + "' or NIPbaru='" + username + "'";
        request.post('http://simpegdashboard.sragenkab.go.id/api/run_all',{ form: { query:query } } ,(error,response,body)=>{
            try {
                if (body.length > 0) {
                    var res = JSON.parse(body)[0];
                    infoPegawai = res;


                    getInfoPegawai(responseBack);
                }else{
                    response.logging(global_username,new Date(),' --> errorParsing');


                    return resolve({
                        success: false,
                        message: "Data tidak ditemukan.",
                        result: null
                    });
                }
            }catch (e){
                console.log(e);
            }
        });
    });
};

function insertToDBPegawai(query=null,responseBack=null){
    response.logging(global_username,new Date(),' --> inserted To DB Pegawai');
    return new Promise((resolve,reject) =>{
        connection.query(query, (err, result) => {
            if(err){
                response.submitLogin(global_username,false,"Gagal Login",responseBack);
            }else{
                if(result.affectedRows > 0){

                    return resolve(getInfoPegawai(responseBack));
                }else{
                    response.submitLogin(global_username,false,"Gagal Login",responseBack);
                }
            }
        });
    });
}

function createSession(nip=null){

    var token = genToken.generate();
    var ex = new Date(Date.now() + 7 * 24 * 60 * 60 * 1000);

    var expire = ex.getFullYear() + '-' + parseInt(ex.getMonth()+1) + '-' + ex.getDate();
    var query = `
                    INSERT INTO maps_user_session (token, hit_count, expire, nip) 
                    VALUES ('`+token+`', 0, '`+expire+`','`+nip+`')`;

    return connection.query(query, function (error, rows, fields) {
        if (error){
            console.log("Session inserted ERROR")
        }else{
            infoSession = {
                'username':nip,
                'token':token
            };
        }

    });
}

function getInfoPegawai(responseBack=null){
    response.logging(global_username,new Date(),' --> getInfoPegawai');
    var resBody = {
        "username":infoSession.username,
        "token":infoSession.token,
        "nip":infoSession.username,
        "nama":infoPegawai.Nama,
        "satker":infoPegawai.SATKER,
        "unor":infoPegawai.UNOR,
        "golru":infoPegawai.GOLRU,
        "pangkat":infoPegawai.PANGKAT
    }
    response.submitLogin(global_username,true,"Berhasil login",resBody,responseBack);
}