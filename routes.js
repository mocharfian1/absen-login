'use strict';

module.exports = function(app) {
    var controller = require('./controller');

    app.route('/')
        .get(controller.index);

    app.route('/submitLogin')
        .get(controller.submitLogin);

    app.route('/getFotoPegawai')
        .get(controller.getFotoPegawai);
};