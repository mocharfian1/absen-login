'use strict';

var response = require('./res');
var model_login = require('./model/model_login');
var connection = require('./conn');
const request = require('request').defaults({rejectUnauthorized:false});;
const TokenGenerator = require('uuid-token-generator');
const genToken = new TokenGenerator();
const axios = require('axios');
const https = require('https');
const httpsAgent = new https.Agent({ rejectUnauthorized: false });
const fs = require('fs');

exports.users = function(req, res) {

    connection.query('SELECT * FROM maps_absen limit 100', function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            console.log("OKE");
            response.ok(rows, res)
        }
    });
};

var RES = null;
exports.submitLogin = function (req, res){
    model_login.setUsername(req.query.username);
    model_login.getSessionInfo(req.query.username,req.query.password,null,res).then((dataSession)=>{
        response.submitLogin(req.query.username,dataSession.success,dataSession.message, dataSession.result, res);
    });
}

exports.index = function(req, res) {
    response.ok("Hello from the Node JS RESTful side!", res)
};

exports.getFotoPegawai = function (req,res){
    const { headers } = req;
    if(headers['x-token-access']){
        if(headers['x-token-access'].length > 0){
            let getToken = headers['x-token-access'];

            var getFoto = new Promise((resolve,reject) => {
                model_login.getUserInfo(getToken).then((data) => {

                    request.get('http://absenku.sragenkab.go.id/images_pegawai/get_images.php?nip=' + data.nip, {}, function (error, response, body) {
                        if (!error && response.statusCode == 200) {
                            var resParse = JSON.parse(body);
                            var resp = {
                                nip:resParse.result.nip,
                                base64:resParse.result.base64
                            }
                            return resolve(resp);
                        }else{
                            return reject("Not Found.");
                        }
                    });

                }).catch((e) => {
                    return reject(e);
                });
            });

            getFoto.then((foto)=>{
                response.resFoto('',true,'Berhasil mendapatkan foto',foto,res);
            }).catch((e)=>{
                response.resFoto("",true,e,"",res);
            });

        }else{
            response.resFoto('',false,"Bad Credentials",'',res);
        }
    }else{
        response.resFoto('',false,"Bad Credentials",'',res);
    }

}
