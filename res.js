'use strict';
const log = require('log4js');
log.configure({
    appenders: { login: { type: "file", filename: "login.log" } },
    categories: { default: { appenders: ["login"], level: "error" } }
});

const logger = log.getLogger('login');

exports.submitLogin = function(username,success, message, resRows, res=null) {
    var data = {
        'success': success,
        'message': message,
        'response': {
            'data': resRows
        }
    };

    this.logging(username,new Date()," --> " + JSON.stringify(data));

    res.json(data);
    res.end();
};

exports.logging = function (username,time,log,type=null){
    if(type === null){
        logger.level = "info";
        logger.info("{'username':'"+username+"','log':'"+log+"'}");
    }
}

exports.resFoto = function (username,success, message, base64, res=null){
    var data = {
        'success': success,
        'message': message,
        'result': base64
    };
    res.json(data);
    res.end();
}